package Models;

import android.content.Context;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.wifimanagement.Constants;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.List;

public class DBHelper extends OrmLiteSqliteOpenHelper {
    private SQLiteDatabase db;

    public DBHelper(Context context) {
        super(context, Constants.MANAGER_DB_NAME, null, Constants.DB_VERSION);
        db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource cs) {
        // Creating Table if not exist.
        try {
            TableUtils.createTableIfNotExists(cs, Device.class);
        } catch (SQLException | java.sql.SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource cs, int oldVersion, int newVersion) {
        // Doing Nothing onUpgrade.
    }

    // Getting all Devices
    public List<Device> getAllDevices(Class className) throws java.sql.SQLException {
        Dao<Device, Object> dao = getDao(className);
        return dao.queryForAll();
    }

    // Update or create entry in table.
    public Dao.CreateOrUpdateStatus createOrUpdate(Device deviceObj) throws java.sql.SQLException {
        Dao<Device, ?> dao = (Dao<Device, ?>)getDao(deviceObj.getClass());
        return dao.createOrUpdate(deviceObj);
    }

    // Deleting Entry in table Devices.
    public void deleteThatEntry(String ssid) throws java.sql.SQLException {
        Dao<Device, ?> dao = (Dao<Device, ?>)getDao(Device.class);
        DeleteBuilder<Device, ?> deleteBuilder = dao.deleteBuilder();
        deleteBuilder.where().eq("ssid", ssid);
        try {
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    // Check if devices table is empty.
    public boolean CheckDeviceTableEmpty() {
        return DatabaseUtils.queryNumEntries(db, Constants.DEVICES_DB_NAME) == 0;
    }
}
