package Models;

import com.google.gson.annotations.SerializedName;

public class RemoveDeviceModel {

    @SerializedName("ID")
    private int id;

    @SerializedName("SSID")
    private String ssid;

    @SerializedName("Device_status")
    private String deviceStatus;

    @SerializedName("Priority")
    private int priority;

    @SerializedName("Password")
    private String password;

    @SerializedName("Security")
    private String security;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSecurity() {
        return security;
    }

    public void setSecurity(String security) {
        this.security = security;
    }
}
