package Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

//  Device Table
@DatabaseTable(tableName = "devices")

// Class for Device Table.
public class Device {
    @DatabaseField(columnName = "id",generatedId = true)
    private int id;

    @DatabaseField(columnName = "db_id")
    private String db_id;

    @DatabaseField(columnName = "ssid")
    private String ssid;

    @DatabaseField(columnName = "security")
    private String security;

    @DatabaseField(columnName = "password")
    private String password;

    @DatabaseField(columnName = "priority")
    private int priority;

    public String getSsid() {
        return this.ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getDb_id() {
        return this.db_id;
    }

    public void setDb_id(String db_id) {
        this.db_id = db_id;
    }

    public void setSecurity(String security) {
        this.security = security;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
