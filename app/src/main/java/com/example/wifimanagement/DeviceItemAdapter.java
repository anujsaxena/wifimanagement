package com.example.wifimanagement;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Models.RemoveDeviceModel;

public class DeviceItemAdapter extends ArrayAdapter<String> {
    int groupid;
    List<RemoveDeviceModel> devices;
    Context context;
    public DeviceItemAdapter(@NonNull Context context, int vg, String[] deviceText, List<RemoveDeviceModel> devices) {
        super(context, vg, deviceText);
        this.context = context;
        groupid = vg;
        this.devices = devices;
    }

    // Hold views of the ListView to improve its scrolling performance
    static class ViewHolder {
        public TextView textview;
        public Switch statusBtn;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        // Inflate the list_item.xml file if convertView is null
        if(rowView==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView= inflater.inflate(groupid, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.textview= rowView.findViewById(R.id.txt);
            viewHolder.statusBtn = rowView.findViewById(R.id.statusSwitch);
            rowView.setTag(viewHolder);

        }

        // Set text to each TextView of ListView item
        final ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.textview.setText(devices.get(position).getSsid());
        if(devices.get(position).getDeviceStatus().equals("active")){
            holder.statusBtn.setChecked(true);
        }
        else{
            holder.statusBtn.setChecked(false);
        }

        holder.statusBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                RequestQueue queue = Volley.newRequestQueue(context);
                final StringRequest stringRequest = new StringRequest(Request.Method.PUT, Constants.URL_POST_DEVICE,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(context, "Device Status Changed", Toast.LENGTH_SHORT).show();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("JSON", "Error in Updating Device Status");
                    }
                }){
                    @Override
                    public Map<String, String> getHeaders(){
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=UTF-8");
                        params.put("Authorization", "Bearer " + Constants.AUTH_TOKEN);
                        return params;
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        String status;
                        if(isChecked){
                            status = "active";
                        }
                        else{
                            status = "inactive";
                        }
                        Map<String,String> params = new HashMap<>();
                        params.put("id", String.valueOf(devices.get(position).getId()));
                        params.put("device_status", status);
                        Log.e("params", params.toString());
                        return new JSONObject(params).toString().getBytes();
                    }
                };
                queue.add(stringRequest);
            }
        });
        return rowView;
    }
}