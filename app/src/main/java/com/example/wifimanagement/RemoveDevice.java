package com.example.wifimanagement;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Models.RemoveDeviceModel;

public class RemoveDevice extends AppCompatActivity {
    HashMap<String, String> map = new HashMap<>();
    HashMap<String, String> statusMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remove_device);

        RequestQueue queue = Volley.newRequestQueue(this);

        // If Network is available then fetch the JSON.
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_GET_ALL_DEVICES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("JSON", "json fetched");
                        Gson gson = new GsonBuilder().create();
                        List<RemoveDeviceModel> devices = Arrays.asList(gson.fromJson(response, RemoveDeviceModel[].class));

                        String[] deviceText = new String[devices.size()];
                        for(int i = 0; i < devices.size(); i++){
                            deviceText[i] = devices.get(i).getSsid();
                        }
                        ListView lstview = findViewById(R.id.devList);
                        DeviceItemAdapter adapter=new DeviceItemAdapter(RemoveDevice.this,R.layout.device, deviceText,devices);
                        lstview.setAdapter(adapter);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("JSON", "Error in changing status of device");
            }
        }){
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + Constants.AUTH_TOKEN);
                return params;
            }
        };
        queue.add(stringRequest);
    }

}
