package com.example.wifimanagement;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class RegisterDevice extends AppCompatActivity {

    private static final String[] wifiSecurityTypes = {"WPA", "WEP"};
    private static final String[] devTypeList = {"dongle", "other devices"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_device);

        // Spinner for Wifi Security
        Spinner security = findViewById(R.id.security);
        ArrayAdapter<String> securityAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, wifiSecurityTypes);
        securityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        security.setAdapter(securityAdapter);

        // Spinner for Wifi Device Type
        Spinner dev_type = findViewById(R.id.dev_type);
        ArrayAdapter<String> deviceTypeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, devTypeList);
        deviceTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dev_type.setAdapter(deviceTypeAdapter);

        Button registerBtn = findViewById(R.id.registerBtn);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(checkForCreation()){
                    addDevice();
                }
                else{
                    Toast.makeText(RegisterDevice.this, "Please Enter all the Fields", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    void addDevice(){
        RequestQueue queue = Volley.newRequestQueue(RegisterDevice.this);
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_POST_DEVICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(RegisterDevice.this, "Device Registered", Toast.LENGTH_SHORT).show();
                        RegisterDevice.this.finish();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("JSON", "Error in creating Device");
            }
        }){
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Authorization", "Bearer " + Constants.AUTH_TOKEN);
                return params;
            }

            @Override
            public byte[] getBody(){
                Map<String,String> params = new HashMap<>();
                EditText ssid = findViewById(R.id.ssid);
                Spinner security = findViewById(R.id.security);
                EditText password = findViewById(R.id.password);
                EditText loc_id = findViewById(R.id.loc_id);
                EditText com_id = findViewById(R.id.com_id);
                EditText priority = findViewById(R.id.priority);
                Spinner dev_type = findViewById(R.id.dev_type);
                params.put("ssid", ssid.getText().toString());
                params.put("security", security.getSelectedItem().toString());
                params.put("password", password.getText().toString());
                params.put("location_id", loc_id.getText().toString());
                params.put("company_id", com_id.getText().toString());
                params.put("priority", priority.getText().toString());
                params.put("device_type", dev_type.getSelectedItem().toString());
                params.put("device_status", returnStatus());
                return new JSONObject(params).toString().getBytes();
            }
        };
        queue.add(stringRequest);
    }

    String returnStatus(){
        Switch status = findViewById(R.id.status);
        if(status.isChecked()){
            return "active";
        }
        else{
            return "inactive";
        }
    }
    boolean checkForCreation(){
        EditText ssid = findViewById(R.id.ssid);
        EditText password = findViewById(R.id.password);
        EditText loc_id = findViewById(R.id.loc_id);
        EditText com_id = findViewById(R.id.com_id);
        EditText priority = findViewById(R.id.priority);
        if(TextUtils.isEmpty(ssid.getText()) || TextUtils.isEmpty(password.getText()) || TextUtils.isEmpty(loc_id.getText()) || TextUtils.isEmpty(com_id.getText()) || TextUtils.isEmpty(priority.getText())){
            return false;
        }
        else{
            return true;
        }
    }
}
