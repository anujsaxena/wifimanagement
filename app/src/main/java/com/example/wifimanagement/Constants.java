package com.example.wifimanagement;

public class Constants {

    // Wifi Delay Constants
    public static final int DELAY_FOR_WIFI_SCAN = 100000;
    public static final int DELAY_FOR_DEVICE_CONNENCTION = 12000;

    // Wifi API Constants
    private static final String SERVER_IP = "192.168.2.140";
    private static final String PORT = "8000";
    public static final String URL_GET_ALL_DEVICES = "http://" + SERVER_IP + ":"+ PORT + "/api/admin_api/access_point_device?view=all";
    public static final String URL_GET_ACTIVE_DEVICES = "http://" + SERVER_IP + ":"+ PORT + "/api/admin_api/access_point_device?view=active";
    public static final String URL_POST_DEVICE = "http://" + SERVER_IP + ":"+ PORT + "/api/admin_api/access_point_device";
    public static final String AUTH_TOKEN = "LdeeyYDISnt6er0ujuDLyJeey7tywL";

    // Database Constants
    public static final String MANAGER_DB_NAME = "device_manager.db";
    public static final String DEVICES_DB_NAME = "devices";
    public static final int DB_VERSION = 1;
}
