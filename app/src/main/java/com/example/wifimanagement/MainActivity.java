package com.example.wifimanagement;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private final int WIFI_PERMISSIONS_ACCESS = 1;
    private static Context context;
    private WifiManager wifiManager;
    WifiReceiver receiver;
    Handler handler = new Handler();


    public static Context getContext(){
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button registerBtn = findViewById(R.id.addDevice);
        registerBtn.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(),   RegisterDevice.class);
                startActivity(intent);
            }
        });

        Button deleteBtn = findViewById(R.id.delDevice);
        deleteBtn.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(),   RemoveDevice.class);
                startActivity(intent);
            }
        });

        // Initializing wifiManager and setting context
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        context = MainActivity.this;

        // Using handler to handel the wifiManager and delay.
        final Runnable wifiRunnable = new Runnable(){
            public void run(){
                wifiManager.startScan();
                handler.postDelayed(this, Constants.DELAY_FOR_WIFI_SCAN);
            }
        };
        handler.postDelayed(wifiRunnable, Constants.DELAY_FOR_WIFI_SCAN);
        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }

        Switch wifiSwitch = findViewById(R.id.wifiSwitch);
        wifiSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(context, "Auto Wifi is on", Toast.LENGTH_SHORT).show();
                    startAgain(wifiRunnable);
                } else {
                    handler.removeCallbacksAndMessages(null);
                    Toast.makeText(context, "Auto Wifi is off", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    // After MainActivity resumed.
    @Override
    protected void onPostResume() {
        super.onPostResume();
        Switch togglewifi = findViewById(R.id.wifiSwitch);
        if(togglewifi.isChecked()){
            receiver = new WifiReceiver(wifiManager);
            IntentFilter iFilter = new IntentFilter();
            iFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
            registerReceiver(receiver, iFilter);
            grantLocPermission();
        }
        else{
            handler.removeCallbacksAndMessages(null);
        }
    }

    private void startAgain(Runnable r){
        handler.removeCallbacks(r);
        handler.postDelayed(r, Constants.DELAY_FOR_WIFI_SCAN);
    }

    // Location Permission Granting
    private void grantLocPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d("Location Permission", "Location Turned off");
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, WIFI_PERMISSIONS_ACCESS);
            } else {
                Log.d("Location Permission", "Location Turned on");
                wifiManager.startScan();
            }
        } else {
            wifiManager.startScan();
        }
    }

    // Requesting Permission for WifiAccess.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case WIFI_PERMISSIONS_ACCESS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("Wifi Permission", "Permission Granted");
                    wifiManager.startScan();
                } else {
                    Log.e("Wifi Permission", "Permission not granted");
                    return;
                }
                break;
        }
    }
}
